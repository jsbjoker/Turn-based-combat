using Game.Attacks.Controllers;
using Game.Attacks.Interfaces;
using Game.Attacks.Views;
using UnityEngine;
using Zenject;

namespace Game.Attacks.Installers
{
    public class AttackInstaller : MonoInstaller
    {
        [SerializeField] private AttackView _attackView;

        public override void InstallBindings()
        {
            Container.Bind<IAttackView>().To<AttackView>().FromInstance(_attackView).AsSingle();
            Container.BindInterfacesTo<AttackHandler>().AsSingle();
            Container.BindInterfacesTo<DamageApplyer>().AsSingle();
        }
    }
}