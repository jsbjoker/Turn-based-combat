using System;
using Game.Characters.Interfaces;

namespace Game.Attacks.Interfaces
{
    public interface IDamageApplyer
    {
        void DoDamage();
        event Action<ICharacter, ICharacter> OnPrepareDamage;
    }
}