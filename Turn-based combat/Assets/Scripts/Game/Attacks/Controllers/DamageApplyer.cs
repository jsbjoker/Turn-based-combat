using System;
using Game.Attacks.Interfaces;
using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.GameProcesses.Interfaces;

namespace Game.Attacks.Controllers
{
    public class DamageApplyer : IDamageApplyer, IDisposable
    {
        private readonly IProcessModel _model;

        public DamageApplyer(IProcessModel model)
        {
            _model = model;
        }
        public event Action<ICharacter, ICharacter> OnPrepareDamage;

        public void DoDamage()
        {
            var from = _model.Turn;
            var to = _model.Enemy;
            OnPrepareDamage?.Invoke(from, to);
            var resultDamage = from.Model[EParam.Damage].ParamValue - from.Model[EParam.Damage].ParamValue * to.Model[EParam.Armor].ParamValue / 100f;
            if (resultDamage <= 0f) return;
            
            to.TakeDamage();
            
            var vampirism = resultDamage * from.Model[EParam.Vampirism].ParamValue / 100f;
            to.Model[EParam.Hp].RemoveValue(resultDamage);
            from.Model[EParam.Hp].AddValue(vampirism);
        }

        public void Dispose()
        {
            OnPrepareDamage = null;
        }
    }
}