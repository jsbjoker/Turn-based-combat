using System;
using Common.Mediators.Initialize.Interfaces;
using Game.Attacks.Interfaces;
using Game.GameProcesses.Enums;
using Game.GameProcesses.Interfaces;

namespace Game.Attacks.Controllers
{
    public class AttackHandler : IAttackHandler, IInitializeListener, IDisposable
    {
        private readonly IAttackView _attackView;
        private readonly IProcessStateMachine _stateMachine;
        private readonly IDamageApplyer _damageApplyer;

        public AttackHandler(IAttackView attackView, IProcessStateMachine stateMachine)
        {
            _attackView = attackView;
            _stateMachine = stateMachine;
            _attackView.OnButtonClick += Attack;
        }

        private void Attack()
        {
            if (_stateMachine.CurrentState != EGameState.StartTurn) return;
            _stateMachine.AdvanceState(EGameState.Turn);
        }

        public void Initialize()
        {
            
        }

        public void Dispose()
        {
            _attackView.OnButtonClick -= Attack;
        }
    }
}