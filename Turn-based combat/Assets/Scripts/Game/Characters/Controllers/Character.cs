using Game.Characters.Enums;
using Game.Characters.Interfaces;

namespace Game.Characters.Controllers
{
    public class Character : ICharacter
    {
        private readonly ICharacterView _characterView;
        private readonly ICharacterStatusPresenter _characterStatusPresenter;
        public IParamModel Model { get; }
        public ETeam Team { get; }

        public Character(ICharacterView characterView, ETeam team, IParamModel model,
            ICharacterStatusPresenter characterStatusPresenter)
        {
            _characterView = characterView;
            _characterStatusPresenter = characterStatusPresenter;
            Team = team;
            Model = model;
        }
        
        public void TakeDamage()
        {
            _characterView.AnimateDamage();
        }

        public void Dispose()
        {
            _characterStatusPresenter.Dispose();
            Model?.Dispose();
        }
    }
}