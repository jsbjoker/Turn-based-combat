using Game.Characters.Behaviours.Interfaces;
using Game.Characters.Enums;
using Game.Characters.Interfaces;

namespace Game.Characters.Controllers
{
    public class CharacterStatusPresenter : ICharacterStatusPresenter
    {
        private readonly IStatusView _statusView;
        private readonly IParamModel _model;

        public CharacterStatusPresenter(IStatusView statusView, IParamModel model)
        {
            _statusView = statusView;
            _model = model;
            _model.OnParamChange += SetCharacterStat;
            Initialize();
        }

        private void Initialize()
        {
            foreach (var param in _model.Params)
                SetCharacterStat(param.Value);
        }
        
        private void SetCharacterStat(IParam param)
        {
            switch (param.Stat)
            {
                case EParam.Hp when param is IClampParam healthParam:
                    _statusView.SetHealth(healthParam.ParamValue, healthParam.MaxValue);
                    break;
                case EParam.Damage:
                    _statusView.SetDamage(param.ParamValue);
                    break;
                case EParam.Armor:
                    _statusView.SetArmor(param.ParamValue);
                    break;
                case EParam.Vampirism:
                    _statusView.SetVampirism(param.ParamValue);
                    break;
            }
        }

        public void Dispose()
        {
            _model.OnParamChange -= SetCharacterStat;
        }
    }
}