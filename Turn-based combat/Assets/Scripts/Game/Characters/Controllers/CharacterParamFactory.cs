using System.Collections.Generic;
using Game.Characters.Behaviours.Controllers;
using Game.Characters.Behaviours.Interfaces;
using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.Characters.Settings;

namespace Game.Characters.Controllers
{
    public class CharacterParamFactory : ICharacterParamFactory
    {
        private readonly CharacterParamsSettings _paramsSettings;

        public CharacterParamFactory(CharacterParamsSettings paramsSettings)
        {
            _paramsSettings = paramsSettings;
        }


        public Dictionary<EParam, Param> CreateParams()
        {
            var defaultParams = new Dictionary<EParam, Param>();
            foreach (var paramType in _paramsSettings.Params)
            {
                var data = _paramsSettings[paramType];
                Param param = data.IsClampValue
                    ? new ClampParam(paramType, data.ClampValues, data.DefaultValue)
                    : new DefaultParam(paramType, data.DefaultValue);
                defaultParams.Add(paramType, param);
            }

            return defaultParams;
        }
    }
}