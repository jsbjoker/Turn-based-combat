using System;
using System.Collections.Generic;
using Game.Characters.Enums;
using Game.Characters.Interfaces;

namespace Game.Characters.Controllers
{
    public class CharacterStorage : ICharacterStorage
    {
        private readonly Dictionary<ETeam, ICharacter> _characters = new();
        private readonly List<ETeam> _turnQueue = new();

        public int CharactersCount => _characters.Count;

        public ETeam this[int id] => _turnQueue[id];

        public ICharacter this[ETeam team] => _characters.ContainsKey(team)
            ? _characters[team]
            : throw new NullReferenceException($"Character with team {team} not found");

        public void AddCharacter(ICharacter character)
        {
            if (_characters.ContainsKey(character.Team))
                throw new Exception($"Character with team {character.Team} already added!");
            
            _characters.Add(character.Team, character);
            _turnQueue.Add(character.Team);
        }

        public void ClearCharacters()
        {
            foreach (var character in _characters)
                character.Value.Dispose();
            
            _characters.Clear();
            _turnQueue.Clear();
        }
    }
}