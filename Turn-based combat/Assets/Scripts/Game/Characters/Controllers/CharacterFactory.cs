using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.Characters.Settings;
using UnityEngine;

namespace Game.Characters.Controllers
{
    public class CharacterFactory : ICharacterFactory
    {
        private readonly CharacterSettings _characterSettings;
        private readonly IStatusProviderView _statusProvider;
        private readonly ICharacterStorage _characterStorage;
        private readonly ICharacterSpawnView _characterSpawnView;
        private readonly ICharacterParamFactory _paramFactory;

        public CharacterFactory(CharacterSettings characterSettings, IStatusProviderView statusProvider,
            ICharacterStorage characterStorage, ICharacterSpawnView characterSpawnView, ICharacterParamFactory paramFactory)
        {
            _characterSettings = characterSettings;
            _statusProvider = statusProvider;
            _characterStorage = characterStorage;
            _characterSpawnView = characterSpawnView;
            _paramFactory = paramFactory;
        }
        
        public void CreateCharacter(ETeam team)
        {
            var view = Object.Instantiate(_characterSettings[team], _characterSpawnView[team]);
            var model = new ParamModel(_paramFactory.CreateParams());
            var statusPresenter = new CharacterStatusPresenter(_statusProvider[team], model);
            var character = new Character(view, team, model, statusPresenter);
            _characterStorage.AddCharacter(character);
        }
    }
}