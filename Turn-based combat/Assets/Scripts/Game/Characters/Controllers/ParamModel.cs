using System;
using System.Collections.Generic;
using System.Linq;
using Game.BuffSystem.Interfaces;
using Game.Characters.Behaviours.Interfaces;
using Game.Characters.Enums;
using Game.Characters.Interfaces;

namespace Game.Characters.Controllers
{
    public class ParamModel : IParamModel, IParamObserver
    {
        private readonly Dictionary<EParam, IParam> _params;
        public IReadOnlyDictionary<EParam, IParam> Params => _params;
        public IParam this[EParam param] => _params.ContainsKey(param)
            ? _params[param]
            : throw new NullReferenceException("Param not found " + param);

        public bool IsAlive => _params.ContainsKey(EParam.Hp) && _params[EParam.Hp].ParamValue > 0f;
        public event Action<IParam> OnParamChange;

        public ParamModel(Dictionary<EParam, Param> defaultParams)
        {
            foreach (var param in defaultParams)
                param.Value.AddObserver(this);
            _params = defaultParams.ToDictionary(k => k.Key, v => (IParam) v.Value);
        }

        public void NotifyListeners(IParam param)
        {
            OnParamChange?.Invoke(param);
        }
        
        public void Dispose()
        {
            OnParamChange = null;
        }
    }
}