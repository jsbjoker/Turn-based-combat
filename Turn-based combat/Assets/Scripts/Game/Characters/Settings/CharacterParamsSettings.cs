using System;
using System.Collections.Generic;
using Game.Characters.Data;
using Game.Characters.Enums;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

namespace Game.Characters.Settings
{
    [CreateAssetMenu(fileName = "CharacterParamsSettings", menuName = "Settings/Game/CharacterParamsSettings")]
    public class CharacterParamsSettings : ScriptableObject
    {
        [SerializeField] private CharacterParamsDictionary _characterParamDictionary;
        public ICollection<EParam> Params => _characterParamDictionary.Keys;

        public ParamData this[EParam param] => _characterParamDictionary.ContainsKey(param)
            ? _characterParamDictionary[param]
            : throw new NullReferenceException($"Param data for {param} not found!");

    }
    
    [Serializable]
    public class CharacterParamsDictionary : SerializableDictionaryBase<EParam, ParamData> {}
}