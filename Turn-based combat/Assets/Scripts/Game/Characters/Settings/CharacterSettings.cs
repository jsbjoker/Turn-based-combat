using System;
using Game.Characters.Enums;
using Game.Characters.Views;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

namespace Game.Characters.Settings
{
    [CreateAssetMenu(fileName = "CharacterSettings", menuName = "Settings/Game/CharacterSettings")]
    public class CharacterSettings : ScriptableObject
    {
        [SerializeField] private CharacterDictionary _characters;

        public CharacterView this[ETeam team] => _characters.ContainsKey(team)
            ? _characters[team]
            : throw new NullReferenceException($"View for {team} not found!");
    }
    
    [Serializable]
    public class CharacterDictionary : SerializableDictionaryBase<ETeam, CharacterView> {}
}