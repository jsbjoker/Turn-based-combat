using System;
using Game.Characters.Enums;
using Game.Characters.Interfaces;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

namespace Game.Characters.Views
{
    public class StatusProviderView : MonoBehaviour, IStatusProviderView
    {
        [SerializeField] private StatusDictionary _statuses;

        public IStatusView this[ETeam team] => _statuses.ContainsKey(team)
            ? _statuses[team]
            : throw new NullReferenceException($"Status for {team} not found!");
    }
    
    [Serializable]
    public class StatusDictionary : SerializableDictionaryBase<ETeam, StatusView> {}
}