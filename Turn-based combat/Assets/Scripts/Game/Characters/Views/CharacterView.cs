using System.Collections;
using Game.Characters.Interfaces;
using UnityEngine;

namespace Game.Characters.Views
{
    public class CharacterView : MonoBehaviour, ICharacterView
    {
        [SerializeField] private SpriteRenderer _spriteRenderer;
        [SerializeField] private float _takeDamageDuration;
        private Coroutine _coroutine;
        public void AnimateDamage()
        {
            if (_coroutine != null)
                StopCoroutine(_coroutine);
            
            _spriteRenderer.color = Color.white;
            _coroutine = StartCoroutine(TakeDamage());
        }

        private IEnumerator TakeDamage()
        {
            var halfDuration = _takeDamageDuration / 2f;
            var timer = 0f;
            
            while (timer < _takeDamageDuration)
            {
                var delta = timer / _takeDamageDuration;
                
                if (timer <= halfDuration)
                    SetLerpColor(Color.white, Color.red, delta / 0.5f);
                else
                    SetLerpColor(Color.red, Color.white, (delta - 0.5f) / 0.5f);

                timer += Time.deltaTime;
                yield return null;
            }
            _spriteRenderer.color = Color.white;
        }

        private void SetLerpColor(Color from, Color to, float delta)
        {
            var lerpedColor = Color.Lerp(from, to, delta);
            _spriteRenderer.color = lerpedColor;
        }
    }
}