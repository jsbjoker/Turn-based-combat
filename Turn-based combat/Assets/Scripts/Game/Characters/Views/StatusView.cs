using Game.Characters.Interfaces;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Characters.Views
{
    public class StatusView : MonoBehaviour, IStatusView
    {
        [SerializeField] private Image _healthBar;
        [SerializeField] private TextMeshProUGUI _healtAmount;
        [SerializeField] private TextMeshProUGUI _armor;
        [SerializeField] private TextMeshProUGUI _vampirism;
        [SerializeField] private TextMeshProUGUI _damage;

        public void SetHealth(float currentValue, float maxValue)
        {
            _healthBar.fillAmount = currentValue / maxValue;
            _healtAmount.text = $"{currentValue:0.##}/{maxValue:0.##}";
        }

        public void SetArmor(float value)
        {
            _armor.text = value.ToString("0.##");
        }

        public void SetVampirism(float value)
        {
            _vampirism.text = value.ToString("0.##");
        }

        public void SetDamage(float damage)
        {
            _damage.text = damage.ToString("0.##");
        }
    }
}