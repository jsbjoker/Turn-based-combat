using System;
using Game.Characters.Enums;
using Game.Characters.Interfaces;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

namespace Game.Characters.Views
{
    public class CharacterSpawnView : MonoBehaviour, ICharacterSpawnView
    {
        [SerializeField] private CharacterSpawnDictionary _characterSpawns;


        public Transform this[ETeam team] => _characterSpawns.ContainsKey(team)
            ? _characterSpawns[team]
            : throw new NullReferenceException($"Spawn for {team} not found!");
    }
    
    [Serializable]
    public class CharacterSpawnDictionary : SerializableDictionaryBase<ETeam, Transform> {}
}