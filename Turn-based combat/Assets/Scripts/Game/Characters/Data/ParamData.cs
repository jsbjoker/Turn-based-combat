using System;
using UnityEngine;

namespace Game.Characters.Data
{
    [Serializable]
    public class ParamData
    {
        public bool IsClampValue;
        public float DefaultValue;
        public Vector2 ClampValues;
    }
}