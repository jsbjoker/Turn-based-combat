namespace Game.Characters.Enums
{
    public enum EParam
    {
        Hp,
        Damage,
        Armor,
        Vampirism
    }
}