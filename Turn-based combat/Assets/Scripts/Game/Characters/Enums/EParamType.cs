namespace Game.Characters.Enums
{
    public enum EParamType
    {
        Default,
        Clamp,
        OverflowClamp
    }
}