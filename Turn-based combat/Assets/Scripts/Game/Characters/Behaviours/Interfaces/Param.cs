using Game.BuffSystem.Interfaces;
using Game.Characters.Enums;

namespace Game.Characters.Behaviours.Interfaces
{
    public abstract class Param : IParam
    {
        private IParamObserver _paramObserver;
        public EParam Stat { get; }
        public abstract float ParamValue { get; }
        public abstract void AddValue(float value);
        public abstract void RemoveValue(float value);

        public void AddObserver(IParamObserver paramObserver)
        {
            _paramObserver = paramObserver;
        }

        protected Param(EParam param)
        {
            Stat = param;
        }

        protected void NotifyChanges()
        {
            _paramObserver.NotifyListeners(this);
        }
    }
}