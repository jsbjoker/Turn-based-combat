using Game.Characters.Enums;

namespace Game.Characters.Behaviours.Interfaces
{
    public interface IParam
    {
        EParam Stat { get; }
        float ParamValue { get; }
        void AddValue(float value);
        void RemoveValue(float value);
    }
}