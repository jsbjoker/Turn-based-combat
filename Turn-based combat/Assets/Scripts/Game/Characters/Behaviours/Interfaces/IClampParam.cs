namespace Game.Characters.Behaviours.Interfaces
{
    public interface IClampParam : IParam
    {
        float MinValue { get; }
        float MaxValue { get; }
        void ChangeMinValue(float minValue);
        void ChangeMaxValue(float maxValue);
    }
}