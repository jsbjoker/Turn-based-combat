using System;
using Game.Characters.Behaviours.Interfaces;
using Game.Characters.Enums;
using UnityEngine;

namespace Game.Characters.Behaviours.Controllers
{
    public class ClampParam : Param, IClampParam
    {
        private Vector2 _clampValues;
        private float _overflowValue;
        public float MaxValue => _clampValues.y;
        public float MinValue => _clampValues.x;
        public override float ParamValue => Math.Clamp(_overflowValue, MinValue, MaxValue);

        public ClampParam(EParam param, Vector2 clampValues, float defaultValue) : base(param)
        {
            _clampValues = clampValues;
            _overflowValue = defaultValue;
        }

        public override void AddValue(float value)
        { 
            _overflowValue += value;
            NotifyChanges();
        }
        public override void RemoveValue(float value)
        { 
            _overflowValue -= value;
            NotifyChanges();
        }
        public void ChangeMinValue(float minValue)
        { 
            _clampValues.x = minValue;
            NotifyChanges();
        }
        public void ChangeMaxValue(float maxValue)
        { 
            _clampValues.y = maxValue;
            NotifyChanges();
        }
    }
}