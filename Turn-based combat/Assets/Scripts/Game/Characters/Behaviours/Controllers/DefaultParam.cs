using Game.Characters.Behaviours.Interfaces;
using Game.Characters.Enums;

namespace Game.Characters.Behaviours.Controllers
{
    public class DefaultParam : Param
    {
        private float _paramValue;
        public override float ParamValue => _paramValue;
        
        public DefaultParam(EParam param, float defaultValue) : base(param)
        {
            _paramValue = defaultValue;
        }
        
        public override void AddValue(float value)
        {
            _paramValue += value;
            NotifyChanges();
        }

        public override void RemoveValue(float value)
        {
            _paramValue -= value;
            NotifyChanges();
        }
    }
}