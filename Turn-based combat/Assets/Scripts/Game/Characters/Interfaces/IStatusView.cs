namespace Game.Characters.Interfaces
{
    public interface IStatusView
    {
        void SetHealth(float currentValue, float maxValue);
        void SetArmor(float value);
        void SetVampirism(float value);
        void SetDamage(float damage);
    }
}