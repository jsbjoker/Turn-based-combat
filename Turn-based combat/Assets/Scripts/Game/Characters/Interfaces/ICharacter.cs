using System;
using Game.Characters.Enums;

namespace Game.Characters.Interfaces
{
    public interface ICharacter : IDisposable
    {
        IParamModel Model { get; }
        ETeam Team { get; }
        void TakeDamage();
    }
}