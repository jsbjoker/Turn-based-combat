using Game.Characters.Enums;

namespace Game.Characters.Interfaces
{
    public interface ICharacterFactory
    {
        void CreateCharacter(ETeam team);
    }
}