using Game.Characters.Enums;
using UnityEngine;

namespace Game.Characters.Interfaces
{
    public interface ICharacterSpawnView
    {
        Transform this[ETeam team] { get; }
    }
}