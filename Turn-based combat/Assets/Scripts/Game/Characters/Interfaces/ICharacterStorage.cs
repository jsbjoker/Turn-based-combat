using Game.Characters.Enums;

namespace Game.Characters.Interfaces
{
    public interface ICharacterStorage
    {
        int CharactersCount { get; }
        ETeam this[int id] { get; }
        ICharacter this[ETeam team] { get; }
        void AddCharacter(ICharacter character);
        void ClearCharacters();
    }
}