using System;
using System.Collections.Generic;
using Game.Characters.Behaviours.Interfaces;
using Game.Characters.Enums;

namespace Game.Characters.Interfaces
{
    public interface IParamModel : IDisposable
    {
        IReadOnlyDictionary<EParam, IParam> Params { get; }
        IParam this[EParam param] { get; }
        bool IsAlive { get; }
        event Action<IParam> OnParamChange;
    }
}