namespace Game.Characters.Interfaces
{
    public interface ICharacterView
    {
        void AnimateDamage();
    }
}