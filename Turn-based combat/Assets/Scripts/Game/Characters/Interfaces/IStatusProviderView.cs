using Game.Characters.Enums;

namespace Game.Characters.Interfaces
{
    public interface IStatusProviderView
    {
        IStatusView this[ETeam team] { get; }
    }
}