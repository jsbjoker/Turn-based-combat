using System.Collections.Generic;
using Game.Characters.Behaviours.Interfaces;
using Game.Characters.Enums;

namespace Game.Characters.Interfaces
{
    public interface ICharacterParamFactory
    {
        Dictionary<EParam, Param> CreateParams();
    }
}