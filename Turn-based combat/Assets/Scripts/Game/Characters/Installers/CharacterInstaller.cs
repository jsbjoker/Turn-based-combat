using Game.Characters.Controllers;
using Game.Characters.Settings;
using Game.Characters.Views;
using UnityEngine;
using Zenject;

namespace Game.Characters.Installers
{
    public class CharacterInstaller : MonoInstaller
    {
        [SerializeField] private CharacterSpawnView _characterSpawnView;
        [SerializeField] private StatusProviderView _statusProviderView;
        [SerializeField] private CharacterSettings _characterSettings;
        [SerializeField] private CharacterParamsSettings _paramsSettings;

        public override void InstallBindings()
        {
            Container.BindInterfacesTo<CharacterSpawnView>().FromComponentInNewPrefab(_characterSpawnView).AsSingle();
            Container.BindInterfacesTo<StatusProviderView>().FromInstance(_statusProviderView).AsSingle();
            Container.BindInstance(_characterSettings).AsSingle();
            Container.BindInstance(_paramsSettings).AsSingle();

            Container.BindInterfacesTo<CharacterFactory>().AsSingle();
            Container.BindInterfacesTo<CharacterStorage>().AsSingle();
            Container.BindInterfacesTo<CharacterParamFactory>().AsSingle();
        }
    }
}