using System;
using Game.Characters.Interfaces;

namespace Game.GameProcesses.Interfaces
{
    public interface IProcessModel
    {
        event Action<int> OnRoundChange;
        event Action<ICharacter> OnTurnChange;
        event Action OnGameEnd;
        int Round { get; }
        bool IsLastTurn { get; }
        ICharacter Turn { get; }
        ICharacter Enemy { get; }
        void NextRound();
        void NextTurn();
        void EndGame();
        void ResetProcess();
    }
}