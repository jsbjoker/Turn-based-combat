using Game.Characters.Enums;

namespace Game.GameProcesses.Interfaces
{
    public interface ITurnView
    {
        void SetTurn(ETeam team);
    }
}