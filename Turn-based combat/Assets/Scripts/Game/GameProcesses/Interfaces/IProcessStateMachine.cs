using Game.GameProcesses.Enums;

namespace Game.GameProcesses.Interfaces
{
    public interface IProcessStateMachine
    {
        EGameState CurrentState { get; }
        void AdvanceState(EGameState state);
    }
}