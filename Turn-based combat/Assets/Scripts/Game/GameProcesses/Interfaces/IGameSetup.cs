namespace Game.GameProcesses.Interfaces
{
    public interface IGameSetup
    {
        void RestartGame();
    }
}