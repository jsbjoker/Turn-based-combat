using Game.GameProcesses.Enums;

namespace Game.GameProcesses.Interfaces
{
    public interface IGameState
    {
        EGameState State { get; }
        void Initialize(IProcessStateMachine stateMachine);
        void Start();
        void AdvanceState();
        void Exit();
    }
}