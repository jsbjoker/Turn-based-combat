namespace Game.GameProcesses.Interfaces
{
    public interface IRoundView
    {
        void SetRound(int round);
    }
}