using Common.Buttons.Interfaces;
using Game.Characters.Enums;

namespace Game.GameProcesses.Interfaces
{
    public interface IEndGameView : IButtonView
    {
        void SetActiveWindow(bool value);
        void SetResultRound(int rounds);
        void SetWinner(ETeam winner);
    }
}