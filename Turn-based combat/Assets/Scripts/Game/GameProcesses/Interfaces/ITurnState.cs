using System;
using Game.Characters.Enums;

namespace Game.GameProcesses.Interfaces
{
    public interface ITurnState : IDisposable
    {
        ETeam Player { get; }
    }
}