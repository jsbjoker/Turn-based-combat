using Common.Buttons.Views;
using Game.Characters.Enums;
using Game.GameProcesses.Interfaces;
using TMPro;
using UnityEngine;

namespace Game.GameProcesses.Views
{
    public class EndGameView : ButtonView, IEndGameView
    {
        [SerializeField] private TextMeshProUGUI _totalRounds;
        [SerializeField] private TextMeshProUGUI _winner;


        public void SetActiveWindow(bool value) => gameObject.SetActive(value);

        public void SetResultRound(int rounds) => _totalRounds.text = "Total rounds: " + rounds;

        public void SetWinner(ETeam winner) => _winner.text = "Winner: " + winner;
    }
}