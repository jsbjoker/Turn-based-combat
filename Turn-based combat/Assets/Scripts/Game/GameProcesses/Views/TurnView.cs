using Game.Characters.Enums;
using Game.GameProcesses.Interfaces;
using TMPro;
using UnityEngine;

namespace Game.GameProcesses.Views
{
    public class TurnView : MonoBehaviour, ITurnView
    {
        [SerializeField] private TextMeshProUGUI _turn;

        public void SetTurn(ETeam team) => _turn.text = team.ToString();
    }
}