using Game.GameProcesses.Interfaces;
using TMPro;
using UnityEngine;

namespace Game.GameProcesses.Views
{
    public class RoundView : MonoBehaviour, IRoundView
    {
        [SerializeField] private TextMeshProUGUI _round;
        public void SetRound(int round) => _round.text = "Round: " + round;
    }
}