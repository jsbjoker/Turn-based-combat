namespace Game.GameProcesses.Enums
{
    public enum EGameState
    {
        None,
        Initialize,
        StartRound,
        EndRound,
        StartTurn,
        Turn,
        EndTurn,
        EndGame
    }
}