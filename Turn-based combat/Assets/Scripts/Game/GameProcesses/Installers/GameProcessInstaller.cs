using Game.GameProcesses.Controllers;
using Game.GameProcesses.Interfaces;
using Game.GameProcesses.States;
using Game.GameProcesses.Views;
using UnityEngine;
using Zenject;

namespace Game.GameProcesses.Installers
{
    public class GameProcessInstaller : MonoInstaller
    {
        [SerializeField] private RoundView _roundView;
        [SerializeField] private EndGameView _endGameView;
        [SerializeField] private RestartButtonView _restartButtonView;
        [SerializeField] private TurnView _turnView;

        public override void InstallBindings()
        {
            Container.BindInterfacesTo<TurnView>().FromInstance(_turnView).AsSingle();
            Container.Bind<IRestartButtonView>().To<RestartButtonView>().FromInstance(_restartButtonView).AsSingle();
            Container.Bind<IEndGameView>().To<EndGameView>().FromInstance(_endGameView).AsSingle();
            Container.BindInterfacesTo<RoundView>().FromInstance(_roundView).AsSingle();
            Container.BindInterfacesTo<RoundPresenter>().AsSingle();
            Container.BindInterfacesTo<ProcessModel>().AsSingle();
            Container.BindInterfacesTo<ProcessStateMachine>().AsSingle();
            Container.BindInterfacesTo<EndGamePresenter>().AsSingle();
            Container.BindInterfacesTo<GameSetup>().AsSingle();
            Container.BindInterfacesTo<RestartGameHandler>().AsSingle();
            Container.BindInterfacesTo<TurnPresenter>().AsSingle();
            
            //States
            Container.BindInterfacesTo<EndGameState>().AsTransient();
            Container.BindInterfacesTo<EndRoundState>().AsTransient();
            Container.BindInterfacesTo<EndTurnState>().AsTransient();
            Container.BindInterfacesTo<InitializeState>().AsTransient();
            Container.BindInterfacesTo<StartRoundState>().AsTransient();
            Container.BindInterfacesTo<StartTurnState>().AsTransient();
            Container.BindInterfacesTo<TurnState>().AsTransient();
        }
    }
}