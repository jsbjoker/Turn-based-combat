using System;
using Common.Mediators.Initialize.Interfaces;
using Game.Characters.Interfaces;
using Game.GameProcesses.Interfaces;

namespace Game.GameProcesses.Controllers
{
    public class TurnPresenter : ITurnPresenter, IInitializeListener, IDisposable
    {
        private readonly IProcessModel _processModel;
        private readonly ITurnView _turnView;

        public TurnPresenter(IProcessModel processModel, ITurnView turnView)
        {
            _processModel = processModel;
            _turnView = turnView;
            _processModel.OnTurnChange += SetupTurn;
        }

        private void SetupTurn(ICharacter turn)
        {
            _turnView.SetTurn(turn.Team);
        }

        public void Initialize()
        {
            
        }
        
        public void Dispose()
        {
            _processModel.OnTurnChange -= SetupTurn;
        }
    }
}