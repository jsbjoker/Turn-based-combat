using System;
using Common.Mediators.Initialize.Interfaces;
using Game.GameProcesses.Interfaces;

namespace Game.GameProcesses.Controllers
{
    public class RestartGameHandler : IRestartGameHandler, IInitializeListener, IDisposable
    {
        private readonly IRestartButtonView _restartButtonView;
        private readonly IGameSetup _gameSetup;

        public RestartGameHandler(IRestartButtonView restartButtonView, IGameSetup gameSetup)
        {
            _restartButtonView = restartButtonView;
            _gameSetup = gameSetup;
            _restartButtonView.OnButtonClick += RestartGame;
        }

        private void RestartGame()
        {
            _gameSetup.RestartGame();
        }

        public void Initialize()
        {
            
        }
        
        public void Dispose()
        {
            _restartButtonView.OnButtonClick -= RestartGame;
        }
    }
}