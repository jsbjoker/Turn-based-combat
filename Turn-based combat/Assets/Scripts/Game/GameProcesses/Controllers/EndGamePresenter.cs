using System;
using Common.Mediators.Initialize.Interfaces;
using Game.GameProcesses.Interfaces;

namespace Game.GameProcesses.Controllers
{
    public class EndGamePresenter : IEndGamePresenter, IInitializeListener, IDisposable
    {   
        private readonly IProcessModel _processModel;
        private readonly IGameSetup _gameSetup;
        private readonly IEndGameView _endGameView;

        public EndGamePresenter(IProcessModel processModel, IGameSetup gameSetup, IEndGameView endGameView)
        {
            _processModel = processModel;
            _gameSetup = gameSetup;
            _endGameView = endGameView;
            _endGameView.OnButtonClick += RestartGame;
            _processModel.OnGameEnd += ShowResult;
        }
        
        public void Initialize()
        {
            _endGameView.SetActiveWindow(false);
        }

        private void ShowResult()
        {
            _endGameView.SetWinner(_processModel.Turn.Team);
            _endGameView.SetResultRound(_processModel.Round);
            _endGameView.SetActiveWindow(true);
        }

        private void RestartGame()
        {
            _gameSetup.RestartGame();
            _endGameView.SetActiveWindow(false);
        }

        public void Dispose()
        {
            _processModel.OnGameEnd -= ShowResult;
            _endGameView.OnButtonClick -= RestartGame;
        }
    }
}