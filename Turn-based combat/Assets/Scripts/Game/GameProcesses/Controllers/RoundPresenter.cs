using System;
using Common.Mediators.Initialize.Interfaces;
using Game.GameProcesses.Interfaces;

namespace Game.GameProcesses.Controllers
{
    public class RoundPresenter : IRoundPresenter, IInitializeListener, IDisposable
    {
        private readonly IProcessModel _processModel;
        private readonly IRoundView _roundView;

        public RoundPresenter(IProcessModel processModel, IRoundView roundView)
        {
            _processModel = processModel;
            _roundView = roundView;
            _processModel.OnRoundChange += OnRoundChange;
        }

        private void OnRoundChange(int round)
        {
            _roundView.SetRound(round);
        }

        public void Initialize()
        {
            
        }

        public void Dispose()
        {
            _processModel.OnRoundChange -= OnRoundChange;
        }
    }
}