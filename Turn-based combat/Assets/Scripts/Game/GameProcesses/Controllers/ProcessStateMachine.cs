using System.Collections.Generic;
using System.Linq;
using Game.GameProcesses.Enums;
using Game.GameProcesses.Interfaces;
using UnityEngine;

namespace Game.GameProcesses.Controllers
{
    public class ProcessStateMachine : IProcessStateMachine
    {
        private readonly Dictionary<EGameState, IGameState> _states;
        private EGameState _currentState = EGameState.None;
        public EGameState CurrentState => _currentState;

        public ProcessStateMachine(List<IGameState> states)
        {
            _states = states.ToDictionary(k => k.State, v => v);
            
            foreach (var state in _states.Values)
                state.Initialize(this);
        }

        public void AdvanceState(EGameState state)
        {
            if (_states.ContainsKey(CurrentState))
                _states[CurrentState]?.Exit();
            
            if (!_states.ContainsKey(state)) return;
            //Debug.Log(state);
            _currentState = state;
            _states[CurrentState].AdvanceState();
            _states[CurrentState]?.Start();
        }
    }
}