using System;
using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.GameProcesses.Interfaces;

namespace Game.GameProcesses.Controllers
{
    public class ProcessModel : IProcessModel, IDisposable
    {
        private readonly ICharacterStorage _characterStorage;
        private int _turnIndex;

        public event Action<int> OnRoundChange;
        public event Action<ICharacter> OnTurnChange;
        public event Action OnGameEnd;

        public bool IsLastTurn => _turnIndex + 1 >= _characterStorage.CharactersCount;
        public ICharacter Turn => _characterStorage[_characterStorage[_turnIndex]];
        public ICharacter Enemy => _characterStorage[_characterStorage[(_turnIndex + 1) % _characterStorage.CharactersCount]];
        public int Round { get; private set; }

        public ProcessModel(ICharacterStorage characterStorage)
        {
            _characterStorage = characterStorage;
        }

        public void EndGame() => OnGameEnd?.Invoke();

        public void ResetProcess()
        {
            SetTurn(0);
            SetRound(1);
        }
        
        public void NextTurn() => SetTurn((_turnIndex + 1) % _characterStorage.CharactersCount);
        public void NextRound() => SetRound(Round + 1);

        private void SetRound(int round)
        {
            Round = round;
            OnRoundChange?.Invoke(Round);
        }

        private void SetTurn(int turn)
        {
            _turnIndex = turn;
            OnTurnChange?.Invoke(Turn);
        }

        public void Dispose()
        {
            OnGameEnd = null;
            OnRoundChange = null;
            OnTurnChange = null;
        }
    }
}