using Common.Mediators.Initialize.Interfaces;
using Game.BuffSystem.Interfaces;
using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.GameProcesses.Enums;
using Game.GameProcesses.Interfaces;

namespace Game.GameProcesses.Controllers
{
    public class GameSetup : IGameSetup, IInitializeListener
    {
        private readonly ICharacterStorage _characterStorage;
        private readonly ICharacterFactory _characterFactory;
        private readonly IBuffDisposer _buffDisposer;
        private readonly IProcessStateMachine _processStateMachine;

        public GameSetup(ICharacterStorage characterStorage, ICharacterFactory characterFactory,
            IBuffDisposer buffDisposer, IProcessStateMachine processStateMachine)
        {
            _characterStorage = characterStorage;
            _characterFactory = characterFactory;
            _buffDisposer = buffDisposer;
            _processStateMachine = processStateMachine;
        }

        public void Initialize()
        {
            InitializeGame();
        }

        private void InitializeGame()
        {
            _characterFactory.CreateCharacter(ETeam.Player1);
            _characterFactory.CreateCharacter(ETeam.Player2);
            _processStateMachine.AdvanceState(EGameState.Initialize);
        }

        public void RestartGame()
        {
            _characterStorage.ClearCharacters();
            _buffDisposer.DisposeBuffs();
            InitializeGame();
        }
    }
}