using Game.GameProcesses.Enums;
using Game.GameProcesses.Interfaces;

namespace Game.GameProcesses.States
{
    public class InitializeState : IGameState
    {
        private IProcessStateMachine _stateMachine;
        private readonly IProcessModel _processModel;
        public EGameState State => EGameState.Initialize;

        public InitializeState(IProcessModel processModel)
        {
            _processModel = processModel;
        }
        
        public void Initialize(IProcessStateMachine stateMachine)
        {
            _stateMachine = stateMachine;
        }

        public void Start()
        {
            
        }

        public void AdvanceState()
        {
            _processModel.ResetProcess();
            _stateMachine.AdvanceState(EGameState.StartRound);
        }

        public void Exit()
        {
            
        }
    }
}