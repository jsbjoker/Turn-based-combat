using Game.Attacks.Interfaces;
using Game.GameProcesses.Enums;
using Game.GameProcesses.Interfaces;

namespace Game.GameProcesses.States
{
    public class TurnState : IGameState
    {
        private IProcessStateMachine _stateMachine;
        private readonly IDamageApplyer _damageApplyer;
        public EGameState State => EGameState.Turn;

        public TurnState(IDamageApplyer damageApplyer)
        {
            _damageApplyer = damageApplyer;
        }
        
        public void Initialize(IProcessStateMachine stateMachine)
        {
            _stateMachine = stateMachine;
        }

        public void Start()
        {
            
        }

        public void AdvanceState()
        {
            _damageApplyer.DoDamage();
            _stateMachine.AdvanceState(EGameState.EndTurn);
        }

        public void Exit()
        {
            
        }
    }
}