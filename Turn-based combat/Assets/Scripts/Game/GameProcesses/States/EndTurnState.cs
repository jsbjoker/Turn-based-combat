using Game.GameProcesses.Enums;
using Game.GameProcesses.Interfaces;

namespace Game.GameProcesses.States
{
    public class EndTurnState : IGameState
    {
        private readonly IProcessModel _processModel;
        private IProcessStateMachine _stateMachine;
        public EGameState State => EGameState.EndTurn;

        public EndTurnState(IProcessModel processModel)
        {
            _processModel = processModel;
        }

        public void Initialize(IProcessStateMachine stateMachine)
        {
            _stateMachine = stateMachine;
        }

        public void Start()
        {
            
        }

        public void AdvanceState()
        {
            if (!IsEnemyAlive)
            {
                _stateMachine.AdvanceState(EGameState.EndGame);
                return;
            }

            if (_processModel.IsLastTurn)
            {
                _stateMachine.AdvanceState(EGameState.EndRound);
                return;
            }
            
            _processModel.NextTurn();
            _stateMachine.AdvanceState(EGameState.StartTurn);
        }
        
        private bool IsEnemyAlive => _processModel.Enemy.Model.IsAlive;

        public void Exit()
        {
            
        }
    }
}