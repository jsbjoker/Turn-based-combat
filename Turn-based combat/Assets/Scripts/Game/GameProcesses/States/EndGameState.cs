using Game.GameProcesses.Enums;
using Game.GameProcesses.Interfaces;

namespace Game.GameProcesses.States
{
    public class EndGameState : IGameState
    {
        private readonly IProcessModel _processModel;
        private IProcessStateMachine _stateMachine;
        public EGameState State => EGameState.EndGame;

        public EndGameState(IProcessModel processModel)
        {
            _processModel = processModel;
        }

        public void Initialize(IProcessStateMachine stateMachine)
        {
            _stateMachine = stateMachine;
        }

        public void Start()
        {
            
        }

        public void AdvanceState()
        {
            _processModel.EndGame();
        }

        public void Exit()
        {
            
        }
    }
}