using Game.GameProcesses.Enums;
using Game.GameProcesses.Interfaces;

namespace Game.GameProcesses.States
{
    public class StartTurnState : IGameState
    {
        private IProcessStateMachine _stateMachine;
        
        public EGameState State => EGameState.StartTurn;
        
        public void Initialize(IProcessStateMachine stateMachine)
        {
            _stateMachine = stateMachine;
        }

        public void Start()
        {
            
        }

        public void AdvanceState()
        {
            
        }

        public void Exit()
        {
            
        }
    }
}