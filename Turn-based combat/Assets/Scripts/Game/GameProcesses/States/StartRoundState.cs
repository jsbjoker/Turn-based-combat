using Game.GameProcesses.Enums;
using Game.GameProcesses.Interfaces;

namespace Game.GameProcesses.States
{
    public class StartRoundState : IGameState
    {
        private IProcessStateMachine _stateMachine;
        public EGameState State => EGameState.StartRound;
        
        public void Initialize(IProcessStateMachine stateMachine)
        {
            _stateMachine = stateMachine;
        }

        public void Start()
        {
            
        }

        public void AdvanceState()
        {
            _stateMachine.AdvanceState(EGameState.StartTurn);
        }

        public void Exit()
        {
            
        }
    }
}