using System;
using Game.BuffSystem.Behaviours;
using Game.BuffSystem.Behaviours.Controllers;
using Game.BuffSystem.Behaviours.Data;
using Game.BuffSystem.Enums;
using Game.BuffSystem.Interfaces;
using Game.BuffSystem.Settings;
using Game.GameProcesses.Interfaces;

namespace Game.BuffSystem.Factories
{
    public class VampirismSelfFactory : IBuffBehaviourFactory
    {
        private readonly BuffParamsSettings _buffParamsSettings;
        private readonly IProcessModel _processModel;
        public EBuffType BuffType => EBuffType.VampirismSelf;

        public VampirismSelfFactory(BuffParamsSettings buffParamsSettings, IProcessModel processModel)
        {
            _buffParamsSettings = buffParamsSettings;
            _processModel = processModel;
        }

        public IBuff CreateBehaviour(IBuffView buffView)
        {
            if (_buffParamsSettings[BuffType] is VampirismSelfData data)
                return new VampirismSelfBehaviour(BuffType, buffView, data, _processModel);
            else
                throw new Exception("Incorrect buff data for " + BuffType);
        }
    }
}