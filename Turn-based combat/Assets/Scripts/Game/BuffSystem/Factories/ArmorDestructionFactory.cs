using System;
using Game.Attacks.Interfaces;
using Game.BuffSystem.Behaviours;
using Game.BuffSystem.Behaviours.Controllers;
using Game.BuffSystem.Behaviours.Data;
using Game.BuffSystem.Enums;
using Game.BuffSystem.Interfaces;
using Game.BuffSystem.Settings;
using Game.GameProcesses.Interfaces;

namespace Game.BuffSystem.Factories
{
    public class ArmorDestructionFactory : IBuffBehaviourFactory
    {
        private readonly IDamageApplyer _damageApplyer;
        private readonly IProcessModel _processModel;
        private readonly BuffParamsSettings _buffParamsSettings;
        public EBuffType BuffType => EBuffType.ArmorDestruction;

        public ArmorDestructionFactory(IDamageApplyer damageApplyer, IProcessModel processModel, BuffParamsSettings buffParamsSettings)
        {
            _damageApplyer = damageApplyer;
            _processModel = processModel;
            _buffParamsSettings = buffParamsSettings;
        }

        public IBuff CreateBehaviour(IBuffView buffView)
        {
            if (_buffParamsSettings[BuffType] is ArmorDestructionData data)
                return new ArmorDestructionBehaviour(BuffType, buffView, data, _processModel, _damageApplyer);
            else
                throw new Exception("Incorrect buff data for " + BuffType);
        }
    }
}