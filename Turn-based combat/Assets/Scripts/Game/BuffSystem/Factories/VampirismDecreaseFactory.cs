using System;
using Game.Attacks.Interfaces;
using Game.BuffSystem.Behaviours;
using Game.BuffSystem.Behaviours.Controllers;
using Game.BuffSystem.Behaviours.Data;
using Game.BuffSystem.Enums;
using Game.BuffSystem.Interfaces;
using Game.BuffSystem.Settings;
using Game.GameProcesses.Interfaces;

namespace Game.BuffSystem.Factories
{
    public class VampirismDecreaseFactory : IBuffBehaviourFactory
    {
        private readonly IDamageApplyer _damageApplyer;
        private readonly BuffParamsSettings _buffParamsSettings;
        private readonly IProcessModel _processModel;
        public EBuffType BuffType => EBuffType.VampirismDecrease;

        public VampirismDecreaseFactory(IDamageApplyer damageApplyer, BuffParamsSettings buffParamsSettings, IProcessModel processModel)
        {
            _damageApplyer = damageApplyer;
            _buffParamsSettings = buffParamsSettings;
            _processModel = processModel;
        }

        public IBuff CreateBehaviour(IBuffView buffView)
        {
            if (_buffParamsSettings[BuffType] is VampirismDecreaseData data)
                return new VampirismDecreaseBehaviour(BuffType, buffView, data, _processModel, _damageApplyer);
            else
                throw new Exception("Incorrect buff data for " + BuffType);
        }
    }
}