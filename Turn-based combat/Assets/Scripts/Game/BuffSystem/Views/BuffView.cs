using Game.BuffSystem.Interfaces;
using TMPro;
using UnityEngine;

namespace Game.BuffSystem.Views
{
    public class BuffView : MonoBehaviour, IBuffView
    {
        [SerializeField] private TextMeshProUGUI _buffName;
        [SerializeField] private TextMeshProUGUI _buffDuration;


        public void SetAmount(int duration) => _buffDuration.text = duration.ToString();
        public void SetBuffName(string buffName) => _buffName.text = buffName;
        public void DestroyView() => Destroy(gameObject);
    }
}