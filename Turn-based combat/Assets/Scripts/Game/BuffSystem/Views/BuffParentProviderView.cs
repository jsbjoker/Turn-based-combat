using System;
using Game.BuffSystem.Interfaces;
using Game.Characters.Enums;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

namespace Game.BuffSystem.Views
{
    public class BuffParentProviderView : MonoBehaviour, IBuffParentProviderView
    {
        [SerializeField] private BuffParentDictionary _parents;

        public RectTransform this[ETeam team] => _parents.ContainsKey(team)
            ? _parents[team]
            : throw new NullReferenceException($"Buff parent for {team} not found!");
    }
    
    [Serializable]
    public class BuffParentDictionary : SerializableDictionaryBase<ETeam, RectTransform> {}
}