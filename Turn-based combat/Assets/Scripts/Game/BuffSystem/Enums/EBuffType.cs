namespace Game.BuffSystem.Enums
{
    public enum EBuffType
    {
        DoubleDamage,
        ArmorSelf,
        ArmorDestruction,
        VampirismSelf,
        VampirismDecrease
    }
}