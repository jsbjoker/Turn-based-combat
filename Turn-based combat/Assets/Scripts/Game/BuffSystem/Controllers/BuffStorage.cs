using System;
using System.Collections.Generic;
using Game.BuffSystem.Interfaces;
using Game.Characters.Enums;

namespace Game.BuffSystem.Controllers
{
    public class BuffStorage : IBuffStorage
    {
        private readonly Dictionary<ETeam, List<IBuff>> _buffs = new();
        public bool ContainBuffs(ETeam team) => _buffs.ContainsKey(team);

        public IReadOnlyList<IBuff> this[ETeam team] => _buffs.ContainsKey(team)
            ? _buffs[team]
            : throw new NullReferenceException($"Buffs for {team} not found");

        public void AddBuff(ETeam team, IBuff buff)
        {
            if (!_buffs.ContainsKey(team))
                _buffs[team] = new List<IBuff>();
            
            _buffs[team].Add(buff);
        }

        public void RemoveBuff(ETeam team, IBuff buff)
        {
            if (!_buffs.ContainsKey(team))
                throw new NullReferenceException($"Cannot delete buff for {team}");
            
            _buffs[team].Remove(buff);
        }

        public void ClearBuffs()
        {
            _buffs.Clear();
        }
    }
}