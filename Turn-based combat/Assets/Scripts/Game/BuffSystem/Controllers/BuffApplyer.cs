using System;
using System.Collections.Generic;
using System.Linq;
using Game.BuffSystem.Enums;
using Game.BuffSystem.Interfaces;
using Game.GameProcesses.Interfaces;

namespace Game.BuffSystem.Controllers
{
    public class BuffApplyer : IBuffApplyer
    {
        private readonly IBuffFactory _buffFactory;
        private readonly IBuffStorage _buffStorage;
        private readonly IProcessModel _processModel;
        private readonly Random _random;
        private readonly IReadOnlyList<EBuffType> _buffs;

        public BuffApplyer(IBuffFactory buffFactory, IBuffStorage buffStorage, IProcessModel processModel)
        {
            _buffFactory = buffFactory;
            _buffStorage = buffStorage;
            _processModel = processModel;
            _buffs = (EBuffType[])Enum.GetValues(typeof(EBuffType));
            _random = new Random();
        }

        public void GiveBuff() => GiveBuff(GetRandomBuff());

        public void GiveBuff(EBuffType buffType)
        {
            var buff = _buffFactory.CreateBuff(buffType);
            buff.AdvanceBuff();
        }

        private EBuffType GetRandomBuff()
        {
            if (!_buffStorage.ContainBuffs(_processModel.Turn.Team))
                return _buffs[_random.Next(0, _buffs.Count)];
            
            var buffs = _buffs.ToList();
            foreach (var buff in _buffStorage[_processModel.Turn.Team])
                buffs.Remove(buff.BuffType);
        
            return buffs[_random.Next(0, buffs.Count)];
        }
    }
}