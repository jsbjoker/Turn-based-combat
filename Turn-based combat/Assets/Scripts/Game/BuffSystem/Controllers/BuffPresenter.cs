using System;
using Common.Mediators.Initialize.Interfaces;
using Game.BuffSystem.Interfaces;
using Game.Characters.Interfaces;
using Game.GameProcesses.Interfaces;

namespace Game.BuffSystem.Controllers
{
    public class BuffPresenter : IBuffPresenter, IInitializeListener, IDisposable
    {
        private readonly IBuffButtonView _buffButtonView;
        private readonly IBuffStorage _buffStorage;
        private readonly IBuffApplyer _buffApplyer;
        private readonly IProcessModel _processModel;

        public BuffPresenter(IBuffButtonView buffButtonView, IBuffStorage buffStorage,
            IBuffApplyer buffApplyer, IProcessModel processModel)
        {
            _buffButtonView = buffButtonView;
            _buffStorage = buffStorage;
            _buffApplyer = buffApplyer;
            _processModel = processModel;
            _buffButtonView.OnButtonClick += GiveBuff;
            _processModel.OnTurnChange += OnTurnChange;
        }

        private void OnTurnChange(ICharacter turn)
        {
            if (!_buffStorage.ContainBuffs(_processModel.Turn.Team))
            {
                _buffButtonView.SetInteractable(true);
                return;
            }
            _buffButtonView.SetInteractable(_buffStorage[_processModel.Turn.Team].Count < 2);
        }

        private void GiveBuff()
        {
            _buffApplyer.GiveBuff();
            _buffButtonView.SetInteractable(_buffStorage[_processModel.Turn.Team].Count < 2);
        }


        public void Initialize()
        {
            
        }

        public void Dispose()
        {
            _processModel.OnTurnChange -= OnTurnChange;
            _buffButtonView.OnButtonClick -= GiveBuff;
        }
    }
}