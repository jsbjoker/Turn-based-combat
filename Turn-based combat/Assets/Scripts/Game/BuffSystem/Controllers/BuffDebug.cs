using Common.Mediators.Update.Interfaces;
using Game.BuffSystem.Enums;
using Game.BuffSystem.Interfaces;
using Game.GameProcesses.Interfaces;
using UnityEngine;

namespace Game.BuffSystem.Controllers
{
    public class BuffDebug : IBuffDebug, IUpdateListener
    {
        private readonly IBuffApplyer _buffApplyer;
        private readonly IProcessModel _processModel;
        private readonly IBuffStorage _buffStorage;

        public BuffDebug(IBuffApplyer buffApplyer, IProcessModel processModel, IBuffStorage buffStorage)
        {
            _buffApplyer = buffApplyer;
            _processModel = processModel;
            _buffStorage = buffStorage;
        }

        public void Update(float deltaTime)
        {
            if (_buffStorage.ContainBuffs(_processModel.Turn.Team) 
                && _buffStorage[_processModel.Turn.Team].Count >= 2) return;
            
            if (Input.GetKeyDown(KeyCode.F1))
                _buffApplyer.GiveBuff(EBuffType.DoubleDamage);
            if (Input.GetKeyDown(KeyCode.F2))
                _buffApplyer.GiveBuff(EBuffType.ArmorSelf);
            if (Input.GetKeyDown(KeyCode.F3))
                _buffApplyer.GiveBuff(EBuffType.ArmorDestruction);
            if (Input.GetKeyDown(KeyCode.F4))
                _buffApplyer.GiveBuff(EBuffType.VampirismSelf);
            if (Input.GetKeyDown(KeyCode.F5))
                _buffApplyer.GiveBuff(EBuffType.VampirismDecrease);
        }
    }
}