using System;
using Game.BuffSystem.Interfaces;
using Game.Characters.Enums;

namespace Game.BuffSystem.Controllers
{
    public class BuffDisposer : IBuffDisposer
    {
        private readonly IBuffStorage _buffStorage;

        public BuffDisposer(IBuffStorage buffStorage)
        {
            _buffStorage = buffStorage;
        }

        public void AddBuff(IBuff buff)
        {
            buff.OnBuffEnd += DisposeBuff;
        }

        private void DisposeBuff(ETeam team, IBuff buff)
        {
            DisposeBuff(buff);
            _buffStorage.RemoveBuff(team, buff);
        }

        private void DisposeBuff(IBuff buff)
        {
            buff.OnBuffEnd -= DisposeBuff;
            buff.Dispose();
        }

        public void DisposeBuffs()
        {
            foreach (ETeam team in Enum.GetValues(typeof(ETeam)))
            {
                if (!_buffStorage.ContainBuffs(team)) continue;
                foreach (var buff in _buffStorage[team])
                {
                    DisposeBuff(buff);
                }
            }
            _buffStorage.ClearBuffs();
        }
    }
}