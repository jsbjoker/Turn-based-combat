using System;
using System.Collections.Generic;
using System.Linq;
using Game.BuffSystem.Enums;
using Game.BuffSystem.Interfaces;
using Game.BuffSystem.Views;
using Game.GameProcesses.Interfaces;
using Object = UnityEngine.Object;

namespace Game.BuffSystem.Controllers
{
    public class BuffFactory : IBuffFactory
    {
        private readonly BuffView _buffView;
        private readonly IBuffStorage _buffStorage;
        private readonly IBuffParentProviderView _buffParentProviderView;
        private readonly IBuffDisposer _buffDisposer;
        private readonly IProcessModel _processModel;
        private readonly Dictionary<EBuffType, IBuffBehaviourFactory> _behaviourFactories;

        public BuffFactory(BuffView buffView, IBuffStorage buffStorage, List<IBuffBehaviourFactory> behaviourFactories,
            IBuffParentProviderView buffParentProviderView, IBuffDisposer buffDisposer, IProcessModel processModel)
        {
            _buffView = buffView;
            _buffStorage = buffStorage;
            _buffParentProviderView = buffParentProviderView;
            _buffDisposer = buffDisposer;
            _processModel = processModel;
            _behaviourFactories = behaviourFactories.ToDictionary(k => k.BuffType, v => v);
        }

        public IBuff CreateBuff(EBuffType buffType)
        {
            if (!_behaviourFactories.ContainsKey(buffType))
                throw new NullReferenceException($"Factory for {buffType} behaviour not found!");
            var target = _processModel.Turn.Team;
            var view = Object.Instantiate(_buffView, _buffParentProviderView[target]);
            view.SetBuffName(buffType.ToString());
            var buff = _behaviourFactories[buffType].CreateBehaviour(view);
            _buffDisposer.AddBuff(buff);
            _buffStorage.AddBuff(target, buff);
            return buff;
        }
    }
}