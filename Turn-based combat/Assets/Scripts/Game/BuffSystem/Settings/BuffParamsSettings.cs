using System;
using Game.BuffSystem.Behaviours.Data;
using Game.BuffSystem.Enums;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

namespace Game.BuffSystem.Settings
{
    [CreateAssetMenu(fileName = "BuffParamsSettings", menuName = "Settings/Game/BuffParamsSettings")]
    public class BuffParamsSettings : ScriptableObject
    {
        [SerializeField] private BuffParamDictionary _buffParams;

        public BuffData this[EBuffType buffType] => _buffParams.ContainsKey(buffType)
            ? _buffParams[buffType]
            : throw new NullReferenceException($"Data for buff {buffType} not found");
    }

    [Serializable]
    public class BuffParamDictionary : SerializableDictionaryBase<EBuffType, BuffData> {}

}