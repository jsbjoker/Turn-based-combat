using Game.BuffSystem.Controllers;
using Game.BuffSystem.Factories;
using Game.BuffSystem.Interfaces;
using Game.BuffSystem.Settings;
using Game.BuffSystem.Views;
using UnityEngine;
using Zenject;

namespace Game.BuffSystem.Installers
{
    public class BuffInstaller : MonoInstaller
    {
        [SerializeField] private BuffView _buffView;
        [SerializeField] private BuffParentProviderView _parentProviderView;
        [SerializeField] private BuffButtonView _buffButtonView;
        [SerializeField] private BuffParamsSettings _buffParamsSettings;
        
        public override void InstallBindings()
        {
            Container.BindInstance(_buffParamsSettings).AsSingle();
            Container.BindInstance(_buffView).AsSingle();
            Container.BindInterfacesTo<BuffParentProviderView>().FromInstance(_parentProviderView).AsSingle();
            Container.Bind<IBuffButtonView>().To<BuffButtonView>().FromInstance(_buffButtonView).AsSingle();
            
            //Controllers
            Container.BindInterfacesTo<BuffApplyer>().AsSingle();
            Container.BindInterfacesTo<BuffFactory>().AsSingle();
            Container.BindInterfacesTo<BuffPresenter>().AsSingle();
            Container.BindInterfacesTo<BuffStorage>().AsSingle();
            Container.BindInterfacesTo<BuffDisposer>().AsSingle();
            Container.BindInterfacesTo<BuffDebug>().AsSingle();
            
            //Factories
            Container.BindInterfacesTo<ArmorDestructionFactory>().AsTransient();
            Container.BindInterfacesTo<ArmorSelfFactory>().AsTransient();
            Container.BindInterfacesTo<DoubleDamageFactory>().AsTransient();
            Container.BindInterfacesTo<VampirismDecreaseFactory>().AsTransient();
            Container.BindInterfacesTo<VampirismSelfFactory>().AsTransient();
        }
    }
}