using System.Collections.Generic;
using Game.Characters.Enums;

namespace Game.BuffSystem.Interfaces
{
    public interface IBuffStorage
    {
        bool ContainBuffs(ETeam team);
        IReadOnlyList<IBuff> this[ETeam team] { get; }
        void AddBuff(ETeam team, IBuff buff);
        void RemoveBuff(ETeam team, IBuff buff);
        void ClearBuffs();
    }
}