using Common.Buttons.Interfaces;

namespace Game.BuffSystem.Interfaces
{
    public interface IBuffButtonView : IButtonView
    {
        void SetInteractable(bool value);
    }
}