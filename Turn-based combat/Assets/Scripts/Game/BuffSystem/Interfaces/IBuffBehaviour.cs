using Game.BuffSystem.Enums;

namespace Game.BuffSystem.Interfaces
{
    public interface IBuffBehaviour
    {
        EBuffType BuffType { get; }
        void AdvanceBuff();
        void RemoveBuff(IBuff buff);
    }
}