namespace Game.BuffSystem.Interfaces
{
    public interface IBuffDisposer
    {
        void AddBuff(IBuff buff);
        void DisposeBuffs();
    }
}