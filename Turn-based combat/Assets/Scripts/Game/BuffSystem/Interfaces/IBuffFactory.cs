using Game.BuffSystem.Enums;

namespace Game.BuffSystem.Interfaces
{
    public interface IBuffFactory
    {
        IBuff CreateBuff(EBuffType buffType);
    }
}