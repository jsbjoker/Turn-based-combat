using System;
using Game.BuffSystem.Enums;
using Game.Characters.Enums;

namespace Game.BuffSystem.Interfaces
{
    public interface IBuff : IDisposable
    {
        EBuffType BuffType { get; }
        void AdvanceBuff();
        event Action<ETeam, IBuff> OnBuffEnd;
    }
}