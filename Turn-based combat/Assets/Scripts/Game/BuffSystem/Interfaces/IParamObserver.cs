using Game.Characters.Behaviours.Interfaces;

namespace Game.BuffSystem.Interfaces
{
    public interface IParamObserver
    {
        void NotifyListeners(IParam param);
    }
}