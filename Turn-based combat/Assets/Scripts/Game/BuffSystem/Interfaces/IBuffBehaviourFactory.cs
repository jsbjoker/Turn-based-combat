using Game.BuffSystem.Enums;

namespace Game.BuffSystem.Interfaces
{
    public interface IBuffBehaviourFactory
    {
        EBuffType BuffType { get; }
        IBuff CreateBehaviour(IBuffView buffView);
    }
}