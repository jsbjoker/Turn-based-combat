using Game.Characters.Enums;
using UnityEngine;

namespace Game.BuffSystem.Interfaces
{
    public interface IBuffParentProviderView
    {
        RectTransform this[ETeam team] { get; }
    }
}