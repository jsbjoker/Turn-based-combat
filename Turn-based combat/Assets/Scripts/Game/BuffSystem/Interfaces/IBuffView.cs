namespace Game.BuffSystem.Interfaces
{
    public interface IBuffView
    {
        void SetAmount(int duration);
        void DestroyView();
    }
}