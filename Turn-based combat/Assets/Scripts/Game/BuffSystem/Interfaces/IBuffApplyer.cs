using Game.BuffSystem.Enums;

namespace Game.BuffSystem.Interfaces
{
    public interface IBuffApplyer
    {
        void GiveBuff();
        void GiveBuff(EBuffType buffType);
    }
}