using Game.Attacks.Interfaces;
using Game.BuffSystem.Behaviours.Data;
using Game.BuffSystem.Enums;
using Game.BuffSystem.Interfaces;
using Game.Characters.Interfaces;
using Game.GameProcesses.Interfaces;

namespace Game.BuffSystem.Behaviours.Interfaces
{
    public abstract class BeforeAttackBuffBehaviour : DefaultBuffBehaviour   
    {
        private readonly IDamageApplyer _damageApplyer;

        public BeforeAttackBuffBehaviour(EBuffType buffType, IBuffView buffView, BuffData buffData, IProcessModel processModel, IDamageApplyer damageApplyer) 
            : base(buffType, buffView, buffData, processModel)
        {
            _damageApplyer = damageApplyer;
            _damageApplyer.OnPrepareDamage += OnPrepareDamage;
        }

        protected abstract void OnPrepareDamage(ICharacter from, ICharacter to);

        public override void AdvanceBuff()
        {
            throw new System.NotImplementedException();
        }

        public override void Dispose()
        {
            _damageApplyer.OnPrepareDamage -= OnPrepareDamage;
            base.Dispose();
        }
    }
}