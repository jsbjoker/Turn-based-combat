using System;
using Game.BuffSystem.Behaviours.Data;
using Game.BuffSystem.Enums;
using Game.BuffSystem.Interfaces;
using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.GameProcesses.Interfaces;

namespace Game.BuffSystem.Behaviours.Interfaces
{
    public abstract class DefaultBuffBehaviour : IBuff
    {
        private readonly IBuffView _buffView;
        private readonly IProcessModel _processModel;
        public EBuffType BuffType { get; }
        protected ICharacter Target { get; }
        private int Duration { get; set; }
        public event Action<ETeam, IBuff> OnBuffEnd;

        protected DefaultBuffBehaviour(EBuffType buffType, IBuffView buffView, BuffData buffData, IProcessModel processModel)
        {
            BuffType = buffType;
            _buffView = buffView;
            _processModel = processModel;
            Target = _processModel.Turn;
            Duration = buffData.Duration;
            _buffView.SetAmount(Duration);
            _processModel.OnRoundChange += OnRoundChange;
        }

        private void OnRoundChange(int round)
        {
            SpendDuration();
        }

        private void SpendDuration()
        {
            Duration--;
            _buffView.SetAmount(Duration);
            if (Duration > 0) return;
            EndBuff();
        }

        protected virtual void EndBuff()
        {
            OnBuffEnd?.Invoke(Target.Team, this);
        }

        public abstract void AdvanceBuff();

        public virtual void Dispose()
        {
            _processModel.OnRoundChange -= OnRoundChange;
            _buffView.DestroyView();
            OnBuffEnd = null;
        }
    }
}