using Game.BuffSystem.Behaviours.Data;
using Game.BuffSystem.Behaviours.Interfaces;
using Game.BuffSystem.Enums;
using Game.BuffSystem.Interfaces;
using Game.Characters.Enums;
using Game.GameProcesses.Interfaces;

namespace Game.BuffSystem.Behaviours.Controllers
{
    public class DoubleDamageBehaviour : DefaultBuffBehaviour
    {
        private DoubleDamageData _doubleDamageData;
        public DoubleDamageBehaviour(EBuffType buffType, IBuffView buffView, DoubleDamageData buffData,
            IProcessModel processModel) : base(buffType, buffView, buffData, processModel)
        {
            _doubleDamageData = buffData;
        }

        protected override void EndBuff()
        {
            var damageParam = Target.Model[EParam.Damage];
            Target.Model[EParam.Damage].RemoveValue(damageParam.ParamValue - Target.Model[EParam.Damage].ParamValue / _doubleDamageData.DamageMultiplier);
            base.EndBuff();
        }

        public override void AdvanceBuff()
        {
            var damageParam = Target.Model[EParam.Damage];
            damageParam.AddValue(damageParam.ParamValue * _doubleDamageData.DamageMultiplier - damageParam.ParamValue);
        }
    }
}