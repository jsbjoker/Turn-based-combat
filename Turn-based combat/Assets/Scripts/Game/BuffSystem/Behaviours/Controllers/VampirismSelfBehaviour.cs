using Game.BuffSystem.Behaviours.Data;
using Game.BuffSystem.Behaviours.Interfaces;
using Game.BuffSystem.Enums;
using Game.BuffSystem.Interfaces;
using Game.Characters.Enums;
using Game.GameProcesses.Interfaces;

namespace Game.BuffSystem.Behaviours.Controllers
{
    public class VampirismSelfBehaviour : DefaultBuffBehaviour
    {
        private VampirismSelfData _vampirismDecreaseData;
        
        public VampirismSelfBehaviour(EBuffType buffType, IBuffView buffView, VampirismSelfData buffData,
            IProcessModel processModel) : base(buffType, buffView, buffData, processModel)
        {
            _vampirismDecreaseData = buffData;
        }

        protected override void EndBuff()
        {
            Target.Model[EParam.Armor].AddValue(_vampirismDecreaseData.SelfArmorReduce);
            Target.Model[EParam.Vampirism].RemoveValue(_vampirismDecreaseData.VampirismIncrease);
            base.EndBuff();
        }

        public override void AdvanceBuff()
        {
            Target.Model[EParam.Armor].RemoveValue(_vampirismDecreaseData.SelfArmorReduce);
            Target.Model[EParam.Vampirism].AddValue(_vampirismDecreaseData.VampirismIncrease);
        }
    }
}