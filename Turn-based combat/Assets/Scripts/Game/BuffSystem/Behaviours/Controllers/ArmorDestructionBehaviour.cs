using Game.Attacks.Interfaces;
using Game.BuffSystem.Behaviours.Data;
using Game.BuffSystem.Behaviours.Interfaces;
using Game.BuffSystem.Enums;
using Game.BuffSystem.Interfaces;
using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.GameProcesses.Interfaces;

namespace Game.BuffSystem.Behaviours.Controllers
{
    public class ArmorDestructionBehaviour : BeforeAttackBuffBehaviour
    {
        private readonly ArmorDestructionData _armorDestructionData;

        public ArmorDestructionBehaviour(EBuffType buffType, IBuffView buffView, ArmorDestructionData buffData,
            IProcessModel processModel, IDamageApplyer damageApplyer) 
            : base(buffType, buffView, buffData, processModel, damageApplyer)
        {
            _armorDestructionData = buffData;
        }

        protected override void OnPrepareDamage(ICharacter from, ICharacter to)
        {
            if (from.Team != Target.Team) return;
            to.Model[EParam.Armor].RemoveValue(_armorDestructionData.ArmorDestruct);
        }

        public override void AdvanceBuff()
        {
            
        }
    }
}