using Game.BuffSystem.Behaviours.Data;
using Game.BuffSystem.Behaviours.Interfaces;
using Game.BuffSystem.Enums;
using Game.BuffSystem.Interfaces;
using Game.Characters.Enums;
using Game.GameProcesses.Interfaces;

namespace Game.BuffSystem.Behaviours.Controllers
{
    public class ArmorSelfBehaviour : DefaultBuffBehaviour
    {
        private readonly ArmorSelfData _armorSelfData;
        
        public ArmorSelfBehaviour(EBuffType buffType, IBuffView buffView, ArmorSelfData buffData,
            IProcessModel processModel): base(buffType, buffView, buffData, processModel)
        {
            _armorSelfData = buffData;
        }

        protected override void EndBuff()
        {
            Target.Model[EParam.Armor].RemoveValue(_armorSelfData.RecivedArmor);
            base.EndBuff();
        }

        public override void AdvanceBuff()
        {
            Target.Model[EParam.Armor].AddValue(_armorSelfData.RecivedArmor);
        }
    }
}