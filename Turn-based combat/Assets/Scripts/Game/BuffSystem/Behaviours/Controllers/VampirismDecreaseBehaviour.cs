using Game.Attacks.Interfaces;
using Game.BuffSystem.Behaviours.Data;
using Game.BuffSystem.Behaviours.Interfaces;
using Game.BuffSystem.Enums;
using Game.BuffSystem.Interfaces;
using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.GameProcesses.Interfaces;

namespace Game.BuffSystem.Behaviours.Controllers
{
    public class VampirismDecreaseBehaviour : BeforeAttackBuffBehaviour
    {
        private readonly VampirismDecreaseData _vampirismDecreaseData;

        public VampirismDecreaseBehaviour(EBuffType buffType, IBuffView buffView,
            VampirismDecreaseData buffData, IProcessModel processModel, IDamageApplyer damageApplyer)
            : base(buffType, buffView, buffData, processModel, damageApplyer)
        {
            _vampirismDecreaseData = buffData;
        }
        
        protected override void OnPrepareDamage(ICharacter from, ICharacter to)
        {
            if (from.Team != Target.Team) return;
            to.Model[EParam.Vampirism].RemoveValue(_vampirismDecreaseData.ReduceVampirism);
        }

        public override void AdvanceBuff()
        {
            
        }
    }
}