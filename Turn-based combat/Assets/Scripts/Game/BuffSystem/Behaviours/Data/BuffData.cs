using UnityEngine;

namespace Game.BuffSystem.Behaviours.Data
{
    public class BuffData : ScriptableObject
    {
        [SerializeField] private int _duration;

        public int Duration => _duration;
    }
}