using UnityEngine;

namespace Game.BuffSystem.Behaviours.Data
{
    [CreateAssetMenu(fileName = "DoubleDamageData", menuName = "Data/Buffs/DoubleDamageData")]
    public class DoubleDamageData : BuffData
    {
        [SerializeField] private float _damageMultiplier;

        public float DamageMultiplier => _damageMultiplier;
    }
}