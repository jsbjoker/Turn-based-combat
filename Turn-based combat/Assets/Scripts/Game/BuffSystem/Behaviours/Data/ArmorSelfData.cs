using UnityEngine;

namespace Game.BuffSystem.Behaviours.Data
{
    [CreateAssetMenu(fileName = "ArmorSelfData", menuName = "Data/Buffs/ArmorSelfData")]
    public class ArmorSelfData : BuffData
    {
        [SerializeField] private float _recivedArmor;

        public float RecivedArmor => _recivedArmor;
    }
}