using UnityEngine;

namespace Game.BuffSystem.Behaviours.Data
{
    [CreateAssetMenu(fileName = "VampirismSelfData", menuName = "Data/Buffs/VampirismSelfData")]
    public class VampirismSelfData : BuffData
    {
        [SerializeField] private float _selfArmorReduce;
        [SerializeField] private float _vampirismIncrease;

        public float SelfArmorReduce => _selfArmorReduce;
        public float VampirismIncrease => _vampirismIncrease;
    }
}