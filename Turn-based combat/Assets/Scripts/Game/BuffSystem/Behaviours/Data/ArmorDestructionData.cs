using UnityEngine;

namespace Game.BuffSystem.Behaviours.Data
{
    [CreateAssetMenu(fileName = "ArmorDestructionData", menuName = "Data/Buffs/ArmorDestructionData")]
    public class ArmorDestructionData : BuffData
    {
        [SerializeField] private float _armorDestruct;

        public float ArmorDestruct => _armorDestruct;
    }
}