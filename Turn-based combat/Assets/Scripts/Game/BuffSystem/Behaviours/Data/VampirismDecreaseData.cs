using UnityEngine;

namespace Game.BuffSystem.Behaviours.Data
{
    [CreateAssetMenu(fileName = "VampirismDecreaseData", menuName = "Data/Buffs/VampirismDecreaseData")]
    public class VampirismDecreaseData : BuffData
    {
        [SerializeField] private float _reduceVampirism;

        public float ReduceVampirism => _reduceVampirism;
    }
}