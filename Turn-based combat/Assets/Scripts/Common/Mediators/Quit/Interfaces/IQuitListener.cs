﻿namespace Common.Mediators.Quit.Interfaces
{
    public interface IQuitListener
    {
        void ApplicationQuit();
    }
}