﻿using System;
using System.Collections.Generic;
using Common.Mediators.Initialize.Interfaces;
using Common.Mediators.Quit.Interfaces;

namespace Common.Mediators.Quit.Controllers
{
    public class QuitMediator : IQuitMediator, IDisposable, IInitializeListener
    {
        private readonly List<IQuitListener> _quitListeners;
        private readonly IQuitView _quitView;

        public QuitMediator(List<IQuitListener> quitListeners, IQuitView quitView)
        {
            _quitListeners = quitListeners;
            _quitView = quitView;
            _quitView.OnGameExit += OnGameExit;
        }
        
        public void Initialize()
        {
        }

        private void OnGameExit()
        {
            foreach (var quitListener in _quitListeners)
            {
                quitListener.ApplicationQuit();
            }
        }

        public void Dispose()
        {
            _quitView.OnGameExit -= OnGameExit;
        }

        
    }
}