﻿using System;
using Common.Mediators.Coroutines.Interfaces;
using Common.Mediators.Pause.Interfaces;
using Common.Mediators.Quit.Interfaces;
using UnityEngine;

namespace Common.Mediators.Views
{
    public class MediatorView : MonoBehaviour, IPauseView, IQuitView, ICoroutineManager
    {
        public event Action<bool> OnGamePaused;
        public event Action OnGameExit;

        private void OnApplicationPause(bool pauseStatus)
        {
            OnGamePaused?.Invoke(pauseStatus);
        }

        private void OnApplicationQuit()
        {
            OnGameExit?.Invoke();
        }

        private void OnDestroy()
        {
            OnGamePaused = null;
            OnGameExit = null;
        }
    }
}