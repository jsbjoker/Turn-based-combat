﻿using System;
using System.Collections.Generic;
using Common.Mediators.Initialize.Interfaces;
using Common.Mediators.Pause.Interfaces;
using UnityEditor;

namespace Common.Mediators.Pause.Controllers
{
    public class PauseMediator : IPauseMediator, IDisposable, IInitializeListener
    {
        private readonly List<IPauseListener> _pauseListeners;
        private readonly IPauseView _pauseView;

        public PauseMediator(List<IPauseListener> pauseListeners, IPauseView pauseView)
        {
            _pauseListeners = pauseListeners;
            _pauseView = pauseView;
            _pauseView.OnGamePaused += OnGamePaused;
#if UNITY_EDITOR
            EditorApplication.pauseStateChanged += OnPauseStateChange;
#endif
        }

#if UNITY_EDITOR
        private void OnPauseStateChange(PauseState pauseState)
        {
            OnGamePaused(pauseState == PauseState.Paused);
        }
#endif

        public void Initialize()
        {
            
        }

        private void OnGamePaused(bool pauseStatus)
        {
            foreach (var pauseListener in _pauseListeners)
            {
                pauseListener.OnApplicationPause(pauseStatus);
            }
        }


        public void Dispose()
        {
#if UNITY_EDITOR
            EditorApplication.pauseStateChanged -= OnPauseStateChange;
#endif
            _pauseView.OnGamePaused -= OnGamePaused;
        }

        
    }
}