﻿namespace Common.Mediators.Initialize.Interfaces
{
    public interface IInitializeListener
    {
        void Initialize();
    }
}