﻿using System.Collections.Generic;
using System.Linq;
using Common.Mediators.Initialize.Interfaces;
using Zenject;

namespace Common.Mediators.Initialize.Controllers
{
    public class InitializeMediator : IInitializeMediator, IInitializable
    {
        private readonly List<IInitializeListener> _initializeListeners;
        private readonly List<IPreInitializeListener> _preInitializeListeners;
        private readonly List<IDataInitialize> _dataInitializes;

        public InitializeMediator(List<IInitializeListener> initializeListeners, List<IPreInitializeListener> preInitializeListeners,
            List<IDataInitialize> dataInitializes)
        {
            _initializeListeners = initializeListeners;
            _preInitializeListeners = preInitializeListeners;
            _dataInitializes = dataInitializes;
        }

        public void Initialize()
        {
            foreach (var dataInitialize in _dataInitializes)
                dataInitialize.SetupData();
            
            var sortedPreInitializes = _preInitializeListeners.OrderBy(x => x.InitializeOrder).ToList();
            
            foreach (var preInitialize in sortedPreInitializes)
            {
                preInitialize.Initialize();
            }
            
            foreach (var initializeListener in _initializeListeners)
            {
                initializeListener.Initialize();
            }
        }
    }
}