﻿using System.Collections.Generic;
using System.Linq;
using Common.Mediators.Update.Interfaces;
using UnityEngine;
using Zenject;

namespace Common.Mediators.Update.Controllers
{
    public class UpdateMediator : IUpdateMediator, ITickable
    {
        private readonly List<IUpdateListener> _updateListeners;

        public UpdateMediator(List<IUpdateListener> updateListeners)
        {
            _updateListeners = updateListeners;
        }
        
        public void AddUpdateListener(IUpdateListener updateListener)
        {
            _updateListeners.Add(updateListener);
        }

        public void RemoveUpdateListener(IUpdateListener updateListener)
        {
            _updateListeners.Remove(updateListener);
        }

        public void Tick()
        {
            foreach (var updateListener in _updateListeners.ToList())
            {
                updateListener.Update(Time.deltaTime);
            }
        }
    }
}