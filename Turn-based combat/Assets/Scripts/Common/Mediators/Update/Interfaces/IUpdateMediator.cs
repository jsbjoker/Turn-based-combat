﻿namespace Common.Mediators.Update.Interfaces
{
    public interface IUpdateMediator
    {
        void AddUpdateListener(IUpdateListener updateListener);
        void RemoveUpdateListener(IUpdateListener updateListener);
    }
}