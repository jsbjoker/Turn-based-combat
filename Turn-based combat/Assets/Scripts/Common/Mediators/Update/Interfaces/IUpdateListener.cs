﻿namespace Common.Mediators.Update.Interfaces
{
    public interface IUpdateListener
    {
        void Update(float deltaTime);
    }
}