using System;

namespace Common.Buttons.Interfaces
{
    public interface IButtonView
    {
        event Action OnButtonClick;
        void SetInteractable(bool value);
    }
}