using System;
using Common.Buttons.Interfaces;
using UnityEngine;
using UnityEngine.UI;

namespace Common.Buttons.Views
{
    public abstract class ButtonView : MonoBehaviour, IButtonView
    {
        [SerializeField] private Button _button;
        
        public event Action OnButtonClick;
        public void SetInteractable(bool value) => _button.interactable = value;

        private void Awake()
        {
            _button.onClick.AddListener(HandleButtonClick);
        }

        public virtual void HandleButtonClick()
        {
            OnButtonClick?.Invoke();
        }

        private void OnDestroy()
        {
            OnButtonClick = null;
            _button.onClick.RemoveListener(HandleButtonClick);
        }
    }
}